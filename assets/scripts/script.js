let upgrades = {
	damage : {
		additionalDamage : 2,
		goldreduction : 3
	}
};

let monsterImage = document.getElementById('monsterImage');

monsterImage.addEventListener('click', () =>{
	if (currentLife > currentDamage) {
		reduceMonsterCurrentLife();
	} else if (currentLife <= currentDamage){
		addMonsterMaxLife();
		resetLife();
		addCurrentGold();
		addLoopsKilled();
	}
});


/*player stats*/

let playerCurrentGold = document.getElementById('playerCurrentGold')
let currentGold = 0;

function addCurrentGold(){
	currentGold += 1;
	playerCurrentGold.textContent = currentGold;
	return currentGold;
};

let playerLoopsKilled = document.getElementById('loopsKilled')
let loopsKilled = 0;

function addLoopsKilled(){
	loopsKilled += 1;
	playerLoopsKilled.textContent = loopsKilled;
	return loopsKilled;
};

/*monster stats*/

let monsterMaxLife = document.getElementById('monsterMaxLife');
let maxLife = 5;
let monsterCurrentLife = document.getElementById('monsterCurrentLife');
let currentLife = maxLife;

monsterCurrentLife.textContent = currentLife;
monsterMaxLife.textContent = maxLife;

function reduceMonsterCurrentLife(){
	currentLife -= currentDamage;
	monsterCurrentLife.textContent = currentLife;
	return currentLife;
}

function resetLife(){
	currentLife = maxLife;
	monsterCurrentLife.textContent = currentLife;
	return currentLife
}

function addMonsterMaxLife(){
	if (loopsKilled % 3 === 2 && loopsKilled >= 2){
		maxLife += 1;
		monsterMaxLife.textContent = maxLife;
		return maxLife;
	}
}

/*upgrades*/
let damageUpgrade = document.getElementById('damageUpgrade')
let playerCurrentDamage = document.getElementById('playerCurrentDamage')
let currentDamage = 1;
let additionalDamage = 0;

damageUpgrade.addEventListener('click',function(){
	if (currentGold >= 3){
		reduceCurrentGold();
		increaseCurrentDamage();
	}
})

let buttonUpgrade = document.getElementById('buttonUpgrade')

buttonUpgrade.addEventListener('click',function(){
	if (currentGold >= 3){
		reduceCurrentGold();
		changeButtons();
	}
})

let backgroundUpgrade = document.getElementById('backgroundUpgrade')
let breakSkill = document.getElementById('break');
let buttonUpgradeArray = [damageUpgrade, buttonUpgrade, backgroundUpgrade, breakSkill]

function changeButtons(){
	for(i = 0; i <= 4; i++){
		buttonUpgradeArray[i].style.backgroundColor = "green";
		buttonUpgradeArray[i].style.color = "white";
		buttonUpgradeArray[i].style.border = "0px";
		buttonUpgradeArray[i].style.padding = "10px";
		buttonUpgradeArray[i].style.borderRadius = "10px";
	}
}

currentBackgroundUpgrade = 0;
let monsterStats = document.getElementById('monsterStats')
let monsterLife = document.getElementById('monsterLife')

backgroundUpgrade.addEventListener('click', () =>{
	changeBackground();
})

function changeBackground(){
	if (loopsKilled >= 10){
		monsterStats.style.background = 'url(assets/images/background.jpeg) right top';
	} else if (loopsKilled >= 5){
		monsterStats.style.background = 'url(assets/images/background2.jpeg) right top';
	}
	monsterStats.style.backgroundSize = 'cover';
	monsterLife.style.color = 'black';

	return currentBackgroundUpgrade;
}

/*url('img_tree.png')*/

function reduceCurrentGold(){
		currentGold -= upgrades['damage']['goldreduction'];
		playerCurrentGold.textContent = currentGold;
		return currentGold;
}

function increaseCurrentDamage(){
		currentDamage += upgrades['damage']['additionalDamage'];
		playerCurrentDamage.textContent = currentDamage;
		return currentDamage;
}

/*skills*/

breakSkill.addEventListener('click', () =>{
	killMonster()
	currentGold -= 10;
	playerCurrentGold.textContent = currentGold;
	return currentGold;
})

function killMonster(){
	addCurrentGold();
	addLoopsKilled();
}